Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lomiri-weather-app
Upstream-Contact: Daniel Frost
Source: https://gitlab.com/ubports/development/apps/lomiri-weather-app

Files: .gitignore
 .gitlab-ci.yml
 AUTHORS
 ChangeLog
 NEWS
 README.md
 RELEASING.md
 TROUBLESHOOTING.md
 src/CMakeLists.txt
 src/app/CMakeLists.txt
 src/app/qml/data/moonphase.js
 src/app/assets/assets.qrc
 src/app/assets/extended-information_chance-of-rain.svg
 src/app/assets/extended-information_chance-of-sleet.svg
 src/app/assets/extended-information_chance-of-snow.svg
 src/app/assets/extended-information_humidity.svg
 src/app/assets/extended-information_moon.svg
 src/app/assets/extended-information_pressure.svg
 src/app/assets/extended-information_sunrise.svg
 src/app/assets/extended-information_sunset.svg
 src/app/assets/extended-information_uv-level.svg
 src/app/assets/extended-information_wind.svg
 src/app/assets/moon-phase-1-new-moon.svg
 src/app/assets/moon-phase-2-waxing-crescent.svg
 src/app/assets/moon-phase-3-first-quarter.svg
 src/app/assets/moon-phase-4-waxing-gibbous.svg
 src/app/assets/moon-phase-5-full-moon.svg
 src/app/assets/moon-phase-6-waning-gibbous.svg
 src/app/assets/moon-phase-7-third-quarter.svg
 src/app/assets/moon-phase-8-waning-crescent.svg
 src/app/assets/rain-radar.svg
 src/app/assets/satellite.svg
 src/app/assets/weather-1drop-symbolic.svg
 src/app/assets/weather-1flake-symbolic.svg
 src/app/assets/weather-2drops-symbolic.svg
 src/app/assets/weather-2flakes-symbolic.svg
 src/app/assets/weather-3drops-big-symbolic.svg
 src/app/assets/weather-3drops-small-symbolic.svg
 src/app/assets/weather-3flakes-big-symbolic.svg
 src/app/assets/weather-3flakes-small-symbolic.svg
 src/app/assets/weather-4drops-symbolic.svg
 src/app/assets/weather-4flakes-symbolic.svg
 src/app/assets/weather-clear-night.svg
 src/app/assets/weather-clear.svg
 src/app/assets/weather-clouds.svg
 src/app/assets/weather-few-clouds-night.svg
 src/app/assets/weather-few-clouds.svg
 src/app/assets/weather-fog.svg
 src/app/assets/weather-overcast.svg
 src/app/assets/weather-showers-scattered.svg
 src/app/assets/weather-showers.svg
 src/app/assets/weather-sleet1-symbolic.svg
 src/app/assets/weather-sleet2-symbolic.svg
 src/app/assets/weather-sleet3-symbolic.svg
 src/app/assets/weather-snow.svg
 src/app/assets/weather-storm.svg
 src/app/qml/weather-app-splash.svg
 src/app/qml/weather-app.svg
 lomiri-weather-app-migrate.py
 lomiri-weather-app.apparmor
 lomiri-weather-app.desktop.in.in
 lomiri-weather-app.url-dispatcher
 manifest.json.in
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 screenshot_readme.png
 tests/CMakeLists.txt
 tests/autopilot/CMakeLists.txt
 tests/autopilot/lomiri_weather_app/CMakeLists.txt
 tests/autopilot/lomiri_weather_app/databases/34e1e542f2f083ff18f537b07a380071.ini
 tests/autopilot/lomiri_weather_app/databases/34e1e542f2f083ff18f537b07a380071.sqlite
 tests/autopilot/lomiri_weather_app/databases/CMakeLists.txt
 tests/autopilot/lomiri_weather_app/databases/for_migration.conf
 tests/autopilot/lomiri_weather_app/databases/legacy/34e1e542f2f083ff18f537b07a380071.ini
 tests/autopilot/lomiri_weather_app/databases/legacy/34e1e542f2f083ff18f537b07a380071.sqlite
 tests/autopilot/lomiri_weather_app/databases/location_added.conf
 tests/autopilot/lomiri_weather_app/files/1.json
 tests/autopilot/lomiri_weather_app/files/2.json
 tests/autopilot/lomiri_weather_app/files/CMakeLists.txt
 tests/autopilot/lomiri_weather_app/files/legacy/1.json
 tests/autopilot/lomiri_weather_app/files/legacy/2.json
 tests/autopilot/lomiri_weather_app/tests/CMakeLists.txt
 tests/manual/2015.weather.ubportsr:weather-tests/jobs/weather-firstrun.pxu
 tests/manual/2015.weather.ubportsr:weather-tests/jobs/weather-forecasts.pxu
 tests/manual/2015.weather.ubportsr:weather-tests/jobs/weather-locations.pxu
 tests/manual/2015.weather.ubportsr:weather-tests/jobs/weather-settings.pxu
 tests/manual/2015.weather.ubportsr:weather-tests/manage.py
 tests/manual/2015.weather.ubportsr:weather-tests/whitelists/weather-firstrun.whitelist
 tests/manual/2015.weather.ubportsr:weather-tests/whitelists/weather-forecasts.whitelist
 tests/manual/2015.weather.ubportsr:weather-tests/whitelists/weather-locations.whitelist
 tests/manual/2015.weather.ubportsr:weather-tests/whitelists/weather-settings.whitelist
Copyright: 2012-2016, Canonical Ltd.
License: GPL-3
Comment:
 Applying same license and same copyright holdership as set for the
 majority of the actual code.

Files: po/*.po
 po/lomiri-weather-app.pot
Copyright: 2012-2016, Canonical Ltd.
  2015, Rosetta Contributors and Canonical Ltd 2015
  2016, Rosetta Contributors and Canonical Ltd 2016
License: GPL-3
Comment:
 Applying same license as set for the majority of the actual code.

Files: src/app/qml/components/CurrentInfo.qml
 src/app/qml/components/CurrentLocation.qml
 src/app/qml/components/DayDelegate.qml
 src/app/qml/components/DayDelegateExtraInfo.qml
 src/app/qml/components/ExpandableListItem.qml
 src/app/qml/components/ForecastDetailsDelegate.qml
 src/app/qml/components/HeaderRow.qml
 src/app/qml/components/HomeGraphic.qml
 src/app/qml/components/HomeHourly.qml
 src/app/qml/components/HomePageEmptyStateComponent.qml
 src/app/qml/components/ListItemActions/CheckBox.qml
 src/app/qml/components/LoadingIndicator.qml
 src/app/qml/components/LocationsListPageEmptyStateComponent.qml
 src/app/qml/components/NetworkErrorStateComponent.qml
 src/app/qml/components/NoAPIKeyErrorStateComponent.qml
 src/app/qml/components/SettingsButton.qml
 src/app/qml/components/StandardListItem.qml
 src/app/qml/data/Storage.qml
 src/app/qml/ui/AddLocationPage.qml
 src/app/qml/ui/LocationPane.qml
 src/app/qml/ui/LocationsListPage.qml
 src/app/qml/ui/SettingsPage.qml
 src/app/qml/ui/settings/DataProviderPage.qml
 src/app/qml/ui/settings/LocationPage.qml
 src/app/qml/ui/settings/RefreshIntervalPage.qml
 src/app/qml/ui/settings/TempUnitPage.qml
 src/app/qml/ui/settings/WindUnitPage.qml
 tests/autopilot/lomiri_weather_app/__init__.py
 tests/autopilot/lomiri_weather_app/tests/__init__.py
 tests/autopilot/lomiri_weather_app/tests/test_add_location_page.py
 tests/autopilot/lomiri_weather_app/tests/test_empty_state.py
 tests/autopilot/lomiri_weather_app/tests/test_home_page.py
 tests/autopilot/lomiri_weather_app/tests/test_locations_page.py
 tests/autopilot/lomiri_weather_app/tests/test_migration.py
 tests/autopilot/lomiri_weather_app/tests/test_settings_page.py
Copyright: 2012-2015, Canonical Ltd.
  2013, Canonical Ltd.
  2013-2015, 2017, Canonical Ltd.
  2013-2015, Canonical Ltd.
  2015, 2017, Canonical Ltd.
  2015, Canonical Ltd.
  2015-2016, Canonical Ltd.
License: GPL-3

Files: src/app/qml/data/WeatherApi.js
 src/app/qml/lomiri-weather-app.qml
Copyright: 2013, Canonical Ltd.
  2015, Canonical Ltd.
  2024, Maciej Sopyło <me@klh.io>
License: GPL-3


Files: src/app/main.cpp
 src/app/qml/qml.qrc
 src/plugin/CMakeLists.txt
 src/plugin/data_day.cpp
 src/plugin/data_day.h
 src/plugin/data_point.cpp
 src/plugin/data_point.h
 src/plugin/open_meteo/open_meteo_weather_provider.cpp
 src/plugin/open_meteo/open_meteo_weather_provider.h
 src/plugin/plugin.cpp
 src/plugin/plugin.h
 src/plugin/provider.cpp
 src/plugin/provider.h
 src/plugin/qmldir
 src/plugin/weather_data_provider.h
Copyright: 2024, Maciej Sopyło <me@klh.io>
License: GPL-3

Files: src/app/config.h.in
 src/app/qml/components/About.qml
 src/app/qml/components/AboutGeneral.qml
 src/app/qml/components/AboutTroubleshooting.qml
 src/app/qml/components/AboutUsage.qml
 src/app/qml/components/OnlineMap.qml
 src/app/qml/data/CitiesList.js
 src/app/qml/ui/settings/RainUnitPage.qml
 src/app/qml/ui/settings/SnowUnitPage.qml
 src/app/qml/ui/settings/TemperatureColorPage.qml
 src/app/qml/ui/settings/ThemePage.qml
 src/app/qml/ui/settings/TodayColorPage.qml
Copyright: 2019-2020, UBports Foundation
  2020, UBports Foundation
  2021, UBports Foundation
  2025, UBports Foundation
License: GPL-3

Files: src/app/qml/components/HeadState/LocationsHeadState.qml
 src/app/qml/components/HeadState/MultiSelectHeadState.qml
 src/app/qml/components/ListItemActions/Remove.qml
 src/app/qml/components/MultiSelectListView.qml
 src/app/qml/components/WeatherListItem.qml
 src/app/qml/components/WeatherListView.qml
Copyright: 2013-2015, 2017, Andrew Hayzen <ahayzen@gmail.com>
  2013-2015, Andrew Hayzen <ahayzen@gmail.com>
  2014-2015, Andrew Hayzen <ahayzen@gmail.com>
  2015, 2017, Andrew Hayzen <ahayzen@gmail.com>
  2016, Andrew Hayzen <ahayzen@gmail.com>
License: GPL-3

Files: src/app/qml/components/FastScroll.js
 src/app/qml/components/FastScroll.qml
Copyright: 2011, Nokia Corporation and/or its subsidiary(-ies).
  2014, Canonical Ltd.
  2014-2015, Canonical Ltd.
License: BSD-3-clause

Files: src/app/qml/data/moment-timezone-with-data-10-year-range.js
 src/app/qml/data/moment-with-locales.js
Copyright: JS Foundation and other contributors
License: Expat

Files: src/plugin/open_meteo/weather_api.fbs
Copyright: 2023-2024, Patrick Zippenfenig
License: Expat

Files: src/app/qml/ui/HomePage.qml
Copyright: 2015-2016, Canonical Ltd.
  2016, Nekhelesh Ramananthan
  2016-2017, Andrew Hayzen
  2019, Joan CiberSheep
  2019, Krille-chan
  2019-2022, Daniel Frost
  2022, Guido Berhoerster
  2024, Maciej Sopyło <me@klh.io>
  2024, Mike Gabriel
License: GPL-3

Files: CMakeLists.txt
Copyright: 2015-2017, Canonical Ltd
  2019, Joan CiberSheep
  2019-2023, Daniel Frost
  2022-2023, Guido Berhoerster
  2023, Ratchanan Srirattanamet
  2024, Maciej Sopyło <me@klh.io>
  2024, Mike Gabriel
License: GPL-3

Files: clickable.yaml
Copyright: 2022, Guido Berhoerster
  2022, Jonatan Hatakeyama Zeidler
  2023, Daniel Frost
  2024, Maciej Sopyło <me@klh.io>
License: GPL-3

Files: src/app/qml/data/suncalc.js
Copyright: 2011-2015, Vladimir Agafonkin
License: BSD-2-clause

Files: .clang-format
Copyright: 2016, Olivier Goffart <ogoffart@woboq.com>
License: BSD-3-clause

Files: debian/*
Copyright: 2013 Canonical Ltd.
  2024-2025, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: LGPL-3

License: GPL-3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
     the names of its contributors may be used to endorse or promote
     products derived from this software without specific prior written
     permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification, are
 permitted provided that the following conditions are met:
 .
    1. Redistributions of source code must retain the above copyright notice, this list of
       conditions and the following disclaimer.
 .
    2. Redistributions in binary form must reproduce the above copyright notice, this list
       of conditions and the following disclaimer in the documentation and/or other materials
       provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
