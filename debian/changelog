lomiri-weather-app (6.1.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase patches 2001 and 2002.
    + Drop 1001_Amend-various-typos.-All-fixes-occur-in-documentatio.patch.
      Applied upstream.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 14 Feb 2025 10:43:03 +0100

lomiri-weather-app (6.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase 2002_mention-Lomiri-in-desktop-file-description.patch.
    + Add 1001_Amend-various-typos.-All-fixes-occur-in-documentatio.patch.
  * debian/rules:
    + Enable all hardening build flags.
  * debian/copyright:
    + Update copyright attributions.
    + Update auto-generated copyright.in file.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 29 Jan 2025 21:23:26 +0100

lomiri-weather-app (6.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase patches 2001, 2002.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 09 Nov 2024 13:05:22 +0100

lomiri-weather-app (6.0.1-1) unstable; urgency=medium

  * New upstream release.
    - Switch weather data provider to Open-Meteo.
  * debian/patches:
    + Drop patches 0001, 0002 and 0003. Not applicable to upstream anymore,
      not required anymore. Also, trivial rebase of patch 2001.
    + Add 2002_mention-Lomiri-in-desktop-file-description.patch. Don't reference
      Ubuntu Touch in .desktop file, reference Lomiri instead.
  * debian/copyright:
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/source/lintian-overrides:
    + Adjust paths to Javascript code files.
  * debian/control:
    + Add to B-D: qtquickcontrols2-5-dev.
    + Add to B-D: qtlocation5-dev and qtpositioning5-dev.
    + Add to B-D: qtwebengine5-dev.
    + Add to B-D: libflatbuffers-dev.
    + Drop from D: qmlscene.
    + Add to D: ${shlibs:Depends}.
    + Switch lomiri-weather-app from arch:all to arch:any.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 25 Sep 2024 08:40:10 +0200

lomiri-weather-app (5.13.5-2) unstable; urgency=medium

  * Re-upload source-only as is.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 14 May 2024 08:17:50 +0200

lomiri-weather-app (5.13.5-1) unstable; urgency=medium

  * Initial release to Debian. (Closes: #1070400).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 04 May 2024 22:29:34 +0200
